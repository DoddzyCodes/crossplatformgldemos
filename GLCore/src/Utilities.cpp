#include "Utilities.h"
#include "PlatformDependant/glwrapper.h"


#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace GLEngine
{
	namespace Utilities
	{
		unsigned int LoadTexture(const std::string& uri, unsigned int& uiTextureFormat, int& iWidth, int& iHeight)
		{
			int iBytesPerPixel;
			unsigned char *data = stbi_load(uri.c_str(), &iWidth, &iHeight, &iBytesPerPixel, 0);
			assert(data != nullptr);

			uiTextureFormat = GL_RGB;

			switch (iBytesPerPixel)
			{
#ifndef __EMSCRIPTEN__
			case 1:
				uiTextureFormat = GL_RED;
				break;
			case 2:
				uiTextureFormat = GL_RG;
				break;
#endif
			case 3:
				uiTextureFormat = GL_RGB;
				break;
			case 4:
				uiTextureFormat = GL_RGBA;
				break;
			default:
				assert("Unknown texture format");
			}


			unsigned int uiTextureID;
			glGenTextures(1, &uiTextureID);
			glBindTexture(GL_TEXTURE_2D, uiTextureID);
			glTexImage2D(GL_TEXTURE_2D, 0, uiTextureFormat, iWidth, iHeight,
				0, uiTextureFormat, GL_UNSIGNED_BYTE, data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

			stbi_image_free(data);

			return uiTextureID;
		}
	}
}