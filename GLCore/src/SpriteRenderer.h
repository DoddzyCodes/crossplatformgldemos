#pragma once
#include <vector>

#include "glm/glm.hpp"

namespace GLEngine
{
    class SpriteRenderer
    {
    public:
        SpriteRenderer();
        ~SpriteRenderer();

        void AddSprite(class Sprite* pSprite);
        void RemoveSprite(class Sprite* pSprite);

        void RenderAllSprites(const glm::mat4& projectionView);

    private:
        std::vector<class Sprite*> m_pSprites;


    };
}
