#include "OrthoCamera.h"

GLEngine::OrthoCamera::OrthoCamera(float fWidth, float fHeight, float fNear, float fFar)
    : m_fWidth(fWidth)
    , m_fHeight(fHeight)
    , m_fNear(fNear)
    , m_fFar(fFar)
{
    SetupOrtho(0, fWidth, fHeight, 0, fNear, fFar);

    //Move the camera
    SetPosition(glm::vec3(0, 0, -fNear));
    LookAt(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
}

void GLEngine::OrthoCamera::Update(double dt)
{
    //Do nothing for the moment
}
