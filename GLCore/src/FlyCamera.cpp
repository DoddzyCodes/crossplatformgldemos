#include "FlyCamera.h"

#include "glm/glm.hpp"


GLEngine::FlyCamera::FlyCamera() : FlyCamera(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), glm::vec3(0, 1, 0))
{
}

GLEngine::FlyCamera::FlyCamera(glm::vec3 location, glm::vec3 lookAt, glm::vec3 upDirection)
{

	LookAt(location, lookAt, upDirection);
}

GLEngine::FlyCamera::~FlyCamera()
{

}



void GLEngine::FlyCamera::Update(double dt)
{
	HandleInput(dt);
}

void GLEngine::FlyCamera::HandleInput(double dt)
{

	//TODO: Add Keyboard controls
	return;
}

