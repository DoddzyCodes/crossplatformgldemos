#include "Sprite.h"
#include "PlatformDependant/glwrapper.h"

#include "RenderData.h"
#include "SpriteRenderer.h"

#include "glm/gtc/type_ptr.hpp"
#include "glm/gtx/transform.hpp"

#include "Utilities.h"

namespace GLEngine
{
    RenderData*       Sprite::sm_pRenderData = nullptr;
    unsigned int      Sprite::sm_uiShaderID = -1;
    unsigned int      Sprite::sm_uiPVMUniformLocation = -1;
    unsigned int      Sprite::sm_uiColourUniformLocation = -1;

    glm::mat4 Sprite::GenerateModelMatrix()
    {
        if (!m_bScaledTransformDirty)
        {
            return m_scaledTransform;
        }
        else
        {
            m_bScaledTransformDirty = false;

            glm::mat4 sizeScale = glm::scale(glm::vec3(m_fSize, m_fSize, 1));
            m_scaledTransform = m_transform * sizeScale;

            return m_scaledTransform;
        }
    }

	Sprite::Sprite(float fSize)
		: m_pRenderer(nullptr)
		, m_fSize(fSize)
		, m_uiTextureID(-1)
		, m_bScaledTransformDirty(true)
		, m_bOwnsTexture(false)
	{
		CreateRenderData();
		CreateSpriteShader();
	}

    Sprite::Sprite(std::string texture, float fSize)
		: Sprite(fSize)
    {
        LoadSpriteImage(texture);
    }


	Sprite::~Sprite()
    {
        if (m_pRenderer)
        {
            m_pRenderer->RemoveSprite(this);
        }

        if (m_uiTextureID != -1)
        {
            glDeleteTextures(1, &m_uiTextureID);
        }
    }

    void Sprite::Update(double dt)
    {

    }

    void Sprite::Render(const glm::mat4& projectionView, bool bBindRenderDataAndShader /*= true*/)
    {
        if (bBindRenderDataAndShader)
        {
            sm_pRenderData->Bind();
            glUseProgram(sm_uiShaderID);
        }

        glm::mat4 modelMatrix = GenerateModelMatrix();
        glm::mat4 PVM = projectionView * modelMatrix;
        glUniformMatrix4fv(sm_uiPVMUniformLocation, 1, false, glm::value_ptr(PVM));
        glUniform4fv(sm_uiColourUniformLocation, 1, glm::value_ptr(m_colour));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiTextureID);


        sm_pRenderData->Render();

        if (bBindRenderDataAndShader)
        {
            sm_pRenderData->Unbind();
            glUseProgram(0);
        }
    }

    void Sprite::BindRenderData()
    {
        sm_pRenderData->Bind();
        glUseProgram(sm_uiShaderID);
    }

    void Sprite::UnbindRenderData()
    {
        sm_pRenderData->Unbind();
        glUseProgram(0);
    }

    void Sprite::SetPosition(const glm::vec3& position)
    {
        m_transform[3] = glm::vec4(position.x, position.y, position.z, 1);
        m_bScaledTransformDirty = true;
    }

    void Sprite::SetSize(float fSize)
    {
        m_fSize = fSize;
        m_bScaledTransformDirty = true;
    }

    void Sprite::SetColour(const glm::vec4& colour)
    {
        m_colour = colour;
        m_bScaledTransformDirty = true;
    }

    void Sprite::CreateRenderData()
    {
        if (sm_pRenderData != nullptr) return; //Already exists

        RenderData* pData = new RenderData();
        pData->GenerateBuffers(RenderData::VBO | RenderData::IBO);

        pData->Bind();

        float fHalfSize = 0.5f;

        float fVertexData[] = {
            -fHalfSize, -fHalfSize, 0, 1, 0, 0,
            -fHalfSize, fHalfSize,  0, 1, 0, 1,
            fHalfSize,  fHalfSize,  0, 1, 1, 1,
            fHalfSize, -fHalfSize,  0, 1, 1, 0,
        };

        unsigned char uiIndexData[] = {
            0, 2, 1,
            0, 3, 2
        };

        glBufferData(GL_ARRAY_BUFFER,
            4 * 6 * sizeof(float), fVertexData, GL_STATIC_DRAW);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            6 * sizeof(unsigned char), uiIndexData, GL_STATIC_DRAW);

        //Position in 0, Colour in 1 (Colour not currently used)
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(sizeof(float) * 4));

        pData->SetIndexCount(6);
        pData->SetNumberOfVertexAttributes(2);

        pData->Unbind();

        sm_pRenderData = pData;
    }

    void Sprite::CreateSpriteShader()
    {
        if (sm_uiShaderID != -1) return; //Already exists

        int progID;

        const char* vsSource =
            "attribute vec4 Position;\n"
            "attribute vec2 TexCoords;\n"
            "uniform mat4 projectionViewWorldMatrix;\n"
            "varying vec2 vTexCoords;\n"
            "void main(void) {\n"
            " gl_Position = projectionViewWorldMatrix * Position;\n"
            " vTexCoords = TexCoords;\n"
            "}\n";
        const char* fsSource =
            "precision mediump float;"
            "varying vec2 vTexCoords;"
            "uniform sampler2D diffuseTexture;"
            "uniform vec4 RenderColour;"
            "void main() {"
            "gl_FragColor = RenderColour * texture2D(diffuseTexture, vTexCoords); }";


        int success = GL_FALSE;
        unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);


        glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
        glCompileShader(vertexShader);

        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
        if (success == GL_FALSE)
        {
            int infoLogLength = 0;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);
            char* infoLog = new char[infoLogLength];

            glGetShaderInfoLog(vertexShader, infoLogLength, 0, infoLog);
            printf("Error: Failed to link shader program!\n");
            printf("%s\n", infoLog);
        }

        glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
        glCompileShader(fragmentShader);

        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
        if (success == GL_FALSE)
        {
            int infoLogLength = 0;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &infoLogLength);
            char* infoLog = new char[infoLogLength];

            glGetShaderInfoLog(fragmentShader, infoLogLength, 0, infoLog);
            printf("Error: Failed to link shader program!\n");
            printf("%s\n", infoLog);
        }

        progID = glCreateProgram();

        glAttachShader(progID, vertexShader);
        glAttachShader(progID, fragmentShader);

        glBindAttribLocation(progID, 0, "Position");
        glBindAttribLocation(progID, 1, "TexCoords");

        glLinkProgram(progID);


        glGetProgramiv(progID, GL_LINK_STATUS, &success);
        if (success == GL_FALSE)
        {
            int infoLogLength = 0;
            glGetProgramiv(progID, GL_INFO_LOG_LENGTH, &infoLogLength);
            char* infoLog = new char[infoLogLength];

            glGetProgramInfoLog(progID, infoLogLength, 0, infoLog);
            printf("Error: Failed to link shader program!\n");
            printf("%s\n", infoLog);
            delete[] infoLog;
        }

        glDeleteShader(fragmentShader);
        glDeleteShader(vertexShader);


        glUseProgram(progID);
        sm_uiPVMUniformLocation = glGetUniformLocation(progID, "projectionViewWorldMatrix");
        sm_uiColourUniformLocation = glGetUniformLocation(progID, "RenderColour");
        glUseProgram(0);

        sm_uiShaderID = progID;
    }

    void Sprite::LoadSpriteImage(std::string imgPath)
    {
		if (m_uiTextureID != -1 && m_bOwnsTexture == true)
		{
			glDeleteTextures(1, &m_uiTextureID);
		}

		unsigned int uiFormat;
		int iWidth,  iHeight;
		m_uiTextureID = Utilities::LoadTexture(imgPath, uiFormat, iWidth, iHeight);

		m_bOwnsTexture = true;
    }

    void Sprite::RegisterRenderer(SpriteRenderer* pSpriteRenderer)
    {
        m_pRenderer = pSpriteRenderer;
    }

	void Sprite::UnregisterRenderer()
    {
        m_pRenderer = nullptr;
    }

	void Sprite::SetSpriteTexture(std::string textureURI)
	{
		LoadSpriteImage(textureURI);
	}

	void Sprite::SetSpriteTexture(unsigned int textureID)
	{
		if (m_uiTextureID != -1 && m_bOwnsTexture)
		{
			glDeleteTextures(1, &m_uiTextureID);
		}

		m_bOwnsTexture = false;
		m_uiTextureID = textureID;
	}

}