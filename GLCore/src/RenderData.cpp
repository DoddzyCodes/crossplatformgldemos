#include "RenderData.h"

#include "PlatformDependant/glwrapper.h"
#include <iostream>

namespace GLEngine
{

RenderData::RenderData()
    : m_vbo(0)
    , m_ibo(0)
    , m_uiNumberOfVertexAttributes(0)
	, m_eIndexBufferType(GL_UNSIGNED_BYTE)
{
}

RenderData::~RenderData()
{
    DestroyBuffers(Buffers::ALL);
}

RenderData::RenderData(const RenderData& a_other)
{
    m_vbo = a_other.m_vbo;
    m_ibo = a_other.m_ibo;
}


RenderData::RenderData(RenderData&& a_other)
{

    m_vbo = a_other.m_vbo;
    m_ibo = a_other.m_ibo;

    a_other.m_vbo = 0;
    a_other.m_ibo = 0;
}

void RenderData::GenerateBuffers(unsigned char a_uiBuffers)
{
    //If any buffers exist of the type we're creating,
    //destroy them so we don't leak memory
    DestroyBuffers(a_uiBuffers);

    if (a_uiBuffers & Buffers::VBO)
    {
        glGenBuffers(1, &m_vbo);
    }

    if (a_uiBuffers & Buffers::IBO)
    {
        glGenBuffers(1, &m_ibo);
    }
}

void RenderData::DestroyBuffers(unsigned char a_uiBuffers)
{
    if (a_uiBuffers & Buffers::VBO && m_vbo > 0)
    {
        glDeleteBuffers(1, &m_vbo);
    }
    if (a_uiBuffers & Buffers::IBO && m_ibo > 0)
    {
        glDeleteBuffers(1, &m_ibo);
    }
}

void RenderData::CheckBuffers(unsigned char& a_uiBuffers)
{
    a_uiBuffers = 0;

    a_uiBuffers |= (m_vbo > 0) ? Buffers::VBO : 0;
    a_uiBuffers |= (m_ibo > 0) ? Buffers::IBO : 0;
}


void RenderData::Bind()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    if (m_ibo > 0) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

    for (unsigned int i = 0; i < m_uiNumberOfVertexAttributes; i++)
    {
        glEnableVertexAttribArray(i);
    }
}

void RenderData::Render()
{
    if (m_ibo)
    {
        glDrawElements(GL_TRIANGLES, m_uiIndexCount, m_eIndexBufferType, 0);
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, m_uiIndexCount);
    }
}

void RenderData::Unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if (m_ibo > 0) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    for (unsigned int i = 0; i < m_uiNumberOfVertexAttributes; i++)
    {
        glDisableVertexAttribArray(i);
    }
}

};