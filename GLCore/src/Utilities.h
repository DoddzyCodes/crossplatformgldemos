#pragma once
#include <string>

namespace GLEngine
{
	namespace Utilities
	{
		extern unsigned int LoadTexture(const std::string& uri, unsigned int& uiTextureFormat, int& iWidth, int& iHeight);
	}


}