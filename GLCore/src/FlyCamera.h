#pragma once
#include  "BaseCamera.h"

namespace GLEngine
{

	class FlyCamera : public BaseCamera
	{

	public:
		FlyCamera();
		FlyCamera(glm::vec3 location, glm::vec3 lookAt, glm::vec3 upDirection);
		~FlyCamera();
		
		virtual void Update(double dt) override;

	private:
		void HandleInput(double dt);
	};

}
