#pragma once
#include "glm/fwd.hpp"
#include "glm/vec3.hpp"

#include <string>


struct GLFWwindow;
namespace GLEngine
{
    class BaseCamera;

    class GLApplication
    {
    public:
		enum MouseState
		{
			NOT_PRESSED,
			JUST_PRESSED,
			HELD
		};

        GLApplication(std::string appName, unsigned int uiWidth = 1280, unsigned int uiHeight = 720, BaseCamera* m_pCamera = nullptr);
        ~GLApplication();

        void Run();

        //Internal use only
        void RunFrame();

        bool GetRunning() const { return m_bRunning; }
        void SetRunning(bool val) { m_bRunning = val; }

        bool ShouldWindowClose() const;
    protected:

        virtual bool Startup() = 0;
        virtual void Shutdown() = 0;

        virtual bool Update(double dt) = 0;
        virtual void Render() = 0;

        unsigned int GetScreenWidth() const { return m_uiScreenWidth; }
        unsigned int GetScreenHeight() const { return m_uiScreenHeight; }

		MouseState GetMouseState(unsigned int uiMouseNumber);

        BaseCamera* GetCamera() { return m_pCamera; }

        void SwapBuffersAndPollEvents();

        std::string m_strAppName;
        GLFWwindow* m_pWindow;

        BaseCamera* m_pCamera;

        glm::vec3 m_clearColour;
    private:
        //You shall not call this!
        GLApplication() {};

        //Initialization helper functions
        bool InitializeOpenGL();

		//Input helper functions
		void UpdateMouseState();

        static void errorCallback(int error, const char *description) {
            fputs(description, stderr);
        }

        unsigned int m_uiScreenWidth;
        unsigned int m_uiScreenHeight;

        double m_fTotalRunTime;
        double m_fDeltaTime;
        bool m_bRunning;

		MouseState m_eMouseState[3];

#ifndef __EMSCRIPTEN__
        unsigned int m_vao;
#endif
    };
}