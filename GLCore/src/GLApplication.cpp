#include "GLApplication.h"
#include "PlatformDependant/glwrapper.h"
#ifdef __EMSCRIPTEN__
#include  <emscripten/emscripten.h>
#endif

#include "BaseCamera.h"

#include <assert.h>
using glm::vec3;
using glm::vec4;
using glm::mat4;
namespace GLEngine
{

    GLApplication::GLApplication(std::string appName, unsigned int uiWidth, unsigned int uiHeight, BaseCamera* pCamera)
        : m_strAppName(appName)
        , m_pCamera(pCamera)
        , m_uiScreenWidth(uiWidth)
        , m_uiScreenHeight(uiHeight)
        , m_fDeltaTime(0.0f)
        , m_bRunning(false)
        , m_clearColour(0.25f, 0.25f, 0.25f)
    {
        m_fTotalRunTime = glfwGetTime();

    }

    GLApplication::~GLApplication()
    {

    }

    bool GLApplication::InitializeOpenGL()
    {
        glfwSetErrorCallback(errorCallback);

        if (!glfwInit()) {
            fputs("Failed to initialize GLFW3!", stderr);
            return false;
        }

        //Ignored by Open GLES2
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        m_pWindow = glfwCreateWindow(m_uiScreenWidth, m_uiScreenHeight, m_strAppName.c_str(), nullptr, nullptr);

        if (m_pWindow == nullptr)
        {
            fputs("Failed to create GLFW3 window!", stderr);
            glfwTerminate();

            return false;
        }

        glfwMakeContextCurrent(m_pWindow);

#ifndef EMSCRIPTEN
        if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
        {
            glfwDestroyWindow(m_pWindow);
            glfwTerminate();
            return false;
        }

        //Also need to bind a vao because of openGL requirements.  To make sure we emulate exactly how the web version will work
        //we'll bind a vao and just forget about it for the length of running
        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

#endif // !EMSCRIPTEN

        glEnable(GL_DEPTH_TEST); // enables the depth buffer
        glEnable(GL_BLEND);
        //glEnable(GL_FRONT_AND_BACK);

        return true;
    }


	void GLApplication::UpdateMouseState()
	{
		for (int i = 0; i < 3; i++)
		{
			int mouseState = glfwGetMouseButton(m_pWindow, i);
			if (mouseState == GLFW_PRESS)
			{
				if (m_eMouseState[i] == MouseState::NOT_PRESSED)
				{
					m_eMouseState[i] = MouseState::JUST_PRESSED;
				}
				else
				{
					m_eMouseState[i] = MouseState::HELD;
				}
			}
			else
			{
				m_eMouseState[i] = MouseState::NOT_PRESSED;
			}
		}
	}

	void GLApplication::Run()
    {
        InitializeOpenGL();

        Startup();

        double dTotalRunTime = glfwGetTime();
        SetRunning(true);


		assert(m_pCamera->GetPerspectiveSet() && "Don't forget to set the cameras perspective!");

#ifndef __EMSCRIPTEN__
        while (!ShouldWindowClose() && GetRunning())
        {
            RunFrame();
        }

        Shutdown();

        glfwDestroyWindow(m_pWindow);
        glfwTerminate();

#else
        emscripten_set_main_loop_arg([](void* pApplication) -> void {
            GLEngine::GLApplication* pApp = (GLEngine::GLApplication*)pApplication;
            pApp->RunFrame();
    }, (void*)this, 0, 1);
#endif



}

    bool GLApplication::ShouldWindowClose() const
    {
        return glfwWindowShouldClose(m_pWindow) != 0;
    }

    void GLApplication::RunFrame()
    {
        double currTime = glfwGetTime();
        double dDeltaTime = currTime - m_fTotalRunTime;
        m_fTotalRunTime = currTime;

		UpdateMouseState();
        Update(dDeltaTime);
        if (GetCamera())
        {
            Render();
        }

        SwapBuffersAndPollEvents();
    }

	GLEngine::GLApplication::MouseState GLApplication::GetMouseState(unsigned int uiMouseNumber)
	{
		if (uiMouseNumber >= 3) return MouseState::NOT_PRESSED;

		return m_eMouseState[uiMouseNumber];
	}

	void GLApplication::SwapBuffersAndPollEvents()
    {
        glfwSwapBuffers(m_pWindow);
        glfwPollEvents();
    }

};
