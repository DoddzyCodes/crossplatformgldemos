#pragma once

#include <string>
#include "glm\glm.hpp"

namespace GLEngine
{

    class Sprite
    {
    public:
		Sprite() = delete;
		Sprite(float fSize);
        Sprite(std::string texture, float fSize);
        ~Sprite();

        void Update(double dt);
        void Render(const glm::mat4& projectionView, bool bBindRenderDataAndShader = true);

        static void BindRenderData();
        static void UnbindRenderData();

        void SetPosition(const glm::vec3& position);
        glm::vec3 GetPosition() const { return m_transform[3]; }

        void SetSize(float fSize);
        float GetSize() const { return m_fSize; }

        void SetColour(const glm::vec4& colour);
        glm::vec4 GetColour() const { return m_colour; }

        void RegisterRenderer(class SpriteRenderer* pSpriteRenderer);
        void UnregisterRenderer();

		void SetSpriteTexture(std::string textureURI);
		void SetSpriteTexture(unsigned int textureID);

    private:
        static void CreateRenderData();
        static void CreateSpriteShader();

        void             LoadSpriteImage(std::string imgPath);

        glm::mat4 GenerateModelMatrix();

        glm::mat4 m_transform;
        glm::mat4 m_scaledTransform;
        float m_fSize;
        bool m_bScaledTransformDirty;
        glm::vec4 m_colour;
        unsigned int m_uiTextureID;
		bool					m_bOwnsTexture;

        static class RenderData* sm_pRenderData;
        static unsigned int sm_uiShaderID;
        static unsigned int sm_uiPVMUniformLocation;
        static unsigned int sm_uiColourUniformLocation;

        class SpriteRenderer* m_pRenderer;
    };

}