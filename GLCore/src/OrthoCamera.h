#pragma once

#include "BaseCamera.h"

namespace GLEngine
{
    class OrthoCamera : public BaseCamera
    {
    public:
        OrthoCamera() = delete;
        OrthoCamera(float fWidth, float fHeight, float fNear, float fFar);


        virtual void Update(double dt) override;

    private:
        float m_fWidth;
        float m_fHeight;
        float m_fFar;
        float m_fNear;
    };
}
