#ifndef _GL_WRAPPER_H_
#define _GL_WRAPPER_H_

#if __EMSCRIPTEN__
#define GLFW_INCLUDE_ES2
#include <emscripten/emscripten.h>
#else
#include "gl_core_3_1.h"
#endif
#include <GLFW/glfw3.h>

#endif
