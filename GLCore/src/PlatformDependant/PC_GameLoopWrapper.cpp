#include "GameLoopWrapper.h"

#include "glwrapper.h"
#include <glm/glm.hpp>
#include "../GLApplication.h"

void DoMainGameLoop(GLEngine::GLApplication* pApplication)
{
    double dTotalRunTime = glfwGetTime();


    pApplication->SetRunning(true);
    while (!pApplication->ShouldWindowClose() && pApplication->GetRunning())
    {

        double currTime = glfwGetTime();
        double dDeltaTime = currTime - dTotalRunTime;
        dTotalRunTime = currTime;
        

        pApplication->Update(dDeltaTime);


        if (pApplication->GetCamera())
        {
            pApplication->Render();
        }


        pApplication->SwapBuffersAndPollEvents();
       
    }
}
