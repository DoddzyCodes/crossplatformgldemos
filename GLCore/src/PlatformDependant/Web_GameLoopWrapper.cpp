#include "GameLoopWrapper.h"

#include "glwrapper.h"
#include <emscripten/emscripten.h>

#include "../GLApplication.h"

void runMainLoop(void* pApplication);

void DoMainGameLoop(GLEngine::GLApplication* pApplication)
{
    emscripten_set_main_loop_arg(runMainLoop, (void*)pApplication, 0, 0);
}

void runMainLoop(void* pArg)
{
    GLEngine::GLApplication* pApplication = (GLEngine::GLApplication*)pArg;

    double dTotalRunTime = glfwGetTime();

    pApplication->SetRunning(true);


    double currTime = glfwGetTime();
    double dDeltaTime = currTime - dTotalRunTime;
    dTotalRunTime = currTime;


    pApplication->Update(dDeltaTime);


    if (pApplication->GetCamera() != nullptr)
    {
        pApplication->Render();
    }


    pApplication->SwapBuffersAndPollEvents();
}

