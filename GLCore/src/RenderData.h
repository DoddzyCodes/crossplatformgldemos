#pragma once

namespace GLEngine
{

	class RenderData
	{
	public:
		RenderData();
		~RenderData();

		//Can't copy render data
		RenderData(const RenderData& other);

		//But can move it
		RenderData(RenderData&& other);

		enum Buffers
		{
			VBO = 1,
			IBO = 2,
			ALL = 3
		};

		void GenerateBuffers(unsigned char uiBuffers);
		void DestroyBuffers(unsigned char  uiBuffers);

		void CheckBuffers(unsigned char& uiBuffers);

		unsigned int GetVBO() const { return m_vbo; }
		unsigned int GetIBO() const { return m_ibo; }

		void Bind();
		void Render();
		void Unbind();

		void SetIndexCount(unsigned int uiCount) { m_uiIndexCount = uiCount; }
		unsigned int GetIndexCount() const { return m_uiIndexCount; }

		unsigned int GetNumberOfVertexAttributes() const { return m_uiNumberOfVertexAttributes; }
		void SetNumberOfVertexAttributes(unsigned int val) { m_uiNumberOfVertexAttributes = val; }

		void SetIndexBufferType(unsigned int eType) { m_eIndexBufferType = eType; }
		unsigned int GetIndexBufferType() const { return m_eIndexBufferType; }
	private:
		unsigned int m_uiIndexCount;

		unsigned int m_vbo;
		unsigned int m_ibo;

		unsigned int m_uiNumberOfVertexAttributes;
	
		unsigned int m_eIndexBufferType;
	};

}
