#include "SpriteRenderer.h"
#include "Sprite.h"

namespace GLEngine
{
    SpriteRenderer::SpriteRenderer()
    {

    }

    SpriteRenderer::~SpriteRenderer()
    {
    }

    void SpriteRenderer::AddSprite(Sprite* pSprite)
    {
        m_pSprites.push_back(pSprite);
        pSprite->RegisterRenderer(this);
    }

    void SpriteRenderer::RemoveSprite(Sprite* pSprite)
    {
        for (auto it = m_pSprites.begin(); it != m_pSprites.end(); ++it)
        {
            if (*it == pSprite)
            {
                pSprite->UnregisterRenderer();
                m_pSprites.erase(it);
                return;
            }
        }
    }

    void SpriteRenderer::RenderAllSprites(const glm::mat4& projectionView)
    {
        Sprite::BindRenderData();
        for (auto it = m_pSprites.begin(); it != m_pSprites.end(); ++it)
        {
            (*it)->Render(projectionView, false);
        }
        Sprite::UnbindRenderData();
    }

}