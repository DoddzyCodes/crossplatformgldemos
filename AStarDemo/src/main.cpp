#include <stdio.h>
#include <stdlib.h>

#include "AStarApplication.h"
#include "OrthoCamera.h"

int main(int argc, char **argv)
{
    GLEngine::OrthoCamera ortho(640, 480, -5.0f, 30.0f);
    AStarApplication aStarApp(&ortho);

    aStarApp.Run();

}