#pragma once
#include "GLApplication.h"

#include "AStarGrid.h"

namespace GLEngine
{
    class Sprite;
    class SpriteRenderer;
}

class AStarApplication : public GLEngine::GLApplication
{

public:
    AStarApplication(GLEngine::BaseCamera* pCamera);

    virtual bool Update(double dt) override;


    virtual void Render() override;

protected:
    virtual bool Startup() override;

	void BuildTileMap();


    virtual void Shutdown() override;

    GLEngine::SpriteRenderer* m_pRenderer;

	class TileMap* m_pTileMap;
	AStarGrid<20, 15>* m_pPathfindGrid;

	std::list<glm::vec2> m_oldPath;


	GLEngine::Sprite* m_pLastHoveredSprite;
	glm::vec4					m_lastHoverSpriteColour;
	GLEngine::Sprite* m_pStartSprite;
	GLEngine::Sprite* m_pEndSprite;

	bool								m_bMouseClicked;

    unsigned int uiTestShader;
	unsigned int m_uiWallTexture;
	unsigned int m_uiFloorTexture;
};