#include "AStarApplication.h"

#include "GLApplication.h"

#include "Sprite.h"
#include "SpriteRenderer.h"

#include "OrthoCamera.h"

#include "PlatformDependant/glwrapper.h"
#include "TileMap.h"
#include "Utilities.h"

#include "AStarGrid.h"

#include <list>

AStarApplication::AStarApplication(GLEngine::BaseCamera* pCamera)
	: GLApplication("Simple Test", 640, 480, pCamera)
	, m_pLastHoveredSprite(nullptr)
	, m_pStartSprite(nullptr)
	, m_pEndSprite(nullptr)
	, m_bMouseClicked(false)
{

}

bool AStarApplication::Update(double dt)
{
    //throw std::logic_error("The method or operation is not implemented.");v

    return true;
}

void AStarApplication::Render()
{
    //float ratio;
    int width, height;

    glfwGetFramebufferSize(m_pWindow, &width, &height);
    //ratio = width / (float)height;
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    double xMousePos, yMousePos;
    glfwGetCursorPos(m_pWindow, &xMousePos, &yMousePos);
    

	GLEngine::Sprite* hoveredSprite = m_pTileMap->GetSpriteAtPixelLocation(glm::vec2(xMousePos, yMousePos));
	if (hoveredSprite && hoveredSprite != m_pStartSprite && hoveredSprite != m_pEndSprite)
	{
		if (m_pLastHoveredSprite)
		{
			m_pLastHoveredSprite->SetColour(m_lastHoverSpriteColour);
		}

		m_lastHoverSpriteColour = hoveredSprite->GetColour();
		hoveredSprite->SetColour(glm::vec4(1, 0, 0, 1));
		m_pLastHoveredSprite = hoveredSprite;
		
	}
	
	if(GetMouseState(GLFW_MOUSE_BUTTON_LEFT) == GLApplication::JUST_PRESSED )
	{
		if (!glfwGetKey(m_pWindow, GLFW_KEY_LEFT_ALT))
		{
			if (m_pStartSprite == nullptr)
			{
				m_pStartSprite = m_pLastHoveredSprite;
				m_pLastHoveredSprite = nullptr;

				m_pStartSprite->SetColour(glm::vec4(0, 1, 0, 1));

			}
			else if (m_pEndSprite == nullptr)
			{
				m_pEndSprite = m_pLastHoveredSprite;
				m_pLastHoveredSprite = nullptr;

				m_pEndSprite->SetColour(glm::vec4(0, 0, 1, 1));

				std::list<glm::vec2> path;
				bool bFoundPath = m_pPathfindGrid->FindPath(
					m_pTileMap->GetTileLocationAtPixelPosition(m_pStartSprite->GetPosition()),
					m_pTileMap->GetTileLocationAtPixelPosition(m_pEndSprite->GetPosition()),
					path);

				if (bFoundPath)
				{
					m_oldPath.clear();

					//Display Path
					for (auto it = path.begin() ; it != path.end(); ++it)
					{
						GLEngine::Sprite* pathSprite = m_pTileMap->GetSpriteAtGridLocation(*it);
						pathSprite->SetColour(glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));

						m_oldPath.push_back(*it);
					}

				}


			}
			else
			{
				//Reset sprites
				m_pStartSprite->SetColour(glm::vec4(1, 1, 1, 1));
				m_pEndSprite->SetColour(glm::vec4(1, 1, 1, 1));

				m_pStartSprite = nullptr;
				m_pEndSprite = nullptr;

				for (auto it = m_oldPath.begin(); it != m_oldPath.end(); ++it)
				{
					GLEngine::Sprite* pathSprite = m_pTileMap->GetSpriteAtGridLocation(*it);
					pathSprite->SetColour(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
				}
			}
		}
		else
		{
			glm::vec2 tileLocation = m_pTileMap->GetTileLocationAtPixelPosition(glm::vec2(xMousePos, yMousePos));

			unsigned int uiCurrentTile = m_pTileMap->GetTileIDAt(tileLocation);
			unsigned int uiNewTile = (uiCurrentTile == 0) ? 1 : 0;
			float fTileCost = (uiNewTile == 1) ? 5.0f : -1.0f;

			m_pTileMap->SetTileIDAt(tileLocation, uiNewTile);
			m_pPathfindGrid->SetTileCost(tileLocation, fTileCost);
		}
		
	}
    m_pRenderer->RenderAllSprites(m_pCamera->GetProjectionView());
}

bool AStarApplication::Startup()
{
	m_pRenderer = new GLEngine::SpriteRenderer();

	BuildTileMap();

	m_pPathfindGrid = new AStarGrid<20, 15>();

	//Remove hardcoding here!!!!
	for (int x = 0; x < 20; x++)
	{
		for (int y= 0; y < 15; y++)
		{
			unsigned int uiTileID = m_pTileMap->GetTileIDAt(glm::vec2(x, y));
			float fCost = (uiTileID == 0) ? -1.0f : 5.0f;
			m_pPathfindGrid->SetTileCost(glm::vec2(x, y), fCost);
		}
	}

	return true;
}

void AStarApplication::BuildTileMap()
{
	m_pTileMap = new TileMap(32, 640, 480);
	m_pTileMap->SetSpriteRenderer(m_pRenderer);

	unsigned int uiTileMap[] = {
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 1, 1, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 0, 0, 0, 0,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 1, 1, 0,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1,0, 1, 0, 0, 0,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0,
			0, 1, 1, 1,0, 1, 1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1,0, 1, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0
	};

	m_pTileMap->SetTileIDs(uiTileMap, 300, false);

	unsigned int uiFormat;
	int iWidth, iHeight;
	m_uiWallTexture = GLEngine::Utilities::LoadTexture("Data/Textures/metal_wall.png", uiFormat, iWidth, iHeight);
	m_uiFloorTexture = GLEngine::Utilities::LoadTexture("Data/Textures/metal_tile.png", uiFormat, iWidth, iHeight);

	m_pTileMap->MapTileIDToTextureID(0, m_uiWallTexture);
	m_pTileMap->MapTileIDToTextureID(1, m_uiFloorTexture);
	m_pTileMap->UpdateTileTextures();

	m_pTileMap->SetTileMapOffset(glm::vec3(16, 16, 0));
}

void AStarApplication::Shutdown()
{
	delete m_pRenderer;
	delete m_pTileMap;

	glDeleteTextures(1, &m_uiFloorTexture);
	glDeleteTextures(1, &m_uiWallTexture);

    //throw std::logic_error("The method or operation is not implemented.");
}

