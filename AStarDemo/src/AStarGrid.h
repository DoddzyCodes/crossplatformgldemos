#pragma once

#include <list>
#include "glm/vec2.hpp"

//TODO: Update openlist to use a non std-linked list for speed


template<int GridWidth, int GridHeight>
class AStarGrid
{
public:
	AStarGrid();
	~AStarGrid();

	void SetTileCost(const glm::vec2& tile, float fCost);

	//TODO: Update to not use vector and used fixed size array
	bool FindPath(const glm::vec2& start, const glm::vec2& end, std::list<glm::vec2>& path);
	void ResetGrid(bool bResetCost);
	
private:
	enum eDirections
	{
		NORTH,
		SOUTH,
		EAST,
		WEST,
		NORTHWEST,
		NORTHEAST,
		SOUTHWEST,
		SOUTHEAST,
		NUM_DIRECTIONS
	};
	struct GridData
	{
		unsigned int x;
		unsigned int y;

		GridData* pParent;
		float fCost;
		float fDistance;
		float fTotalGridValue;

		bool bIsOnOpenList;
		bool bIsOnClosedList;
	};

	void				AddToOpenList(GridData* data);
	bool				RemoveFromOpenList(GridData* data);
	GridData* PopBestFromOpenList();

	void				AddToClosedList(GridData* data);
	bool				IsOnClosedList(GridData* data);

	void				BuildPath(std::list<glm::vec2>& path, GridData* pEndNode);

	void				GetConnectedNodes(const GridData* pData, GridData** pNodeMap);
	

	GridData* m_openList[GridWidth * GridHeight];


	GridData m_pathFindGrid[GridWidth][GridHeight];

	unsigned int m_uiOpenListSize = 0;

};

#include "AStarGrid.inl"
