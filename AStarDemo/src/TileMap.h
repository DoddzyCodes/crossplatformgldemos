#pragma  once
#include <vector>
#include <map>

#include "glm/glm.hpp"

namespace GLEngine
{
	class SpriteRenderer;
	class Sprite;
}

//TODO: Flatten tilemaps into single sprites if possible
//Emscripten is beginning to hate me already

class TileMap
{
public:
	TileMap() = delete;
	TileMap(float fTileSize, float fMapWidth, float fMapHeight);
	void SetSpriteRenderer(class GLEngine::SpriteRenderer* pRenderer);

	void MapTileIDToTextureID(unsigned int uiTileID, unsigned int uiTextureID);

	void SetTileIDs(const unsigned int* pTileIDs, unsigned int uiLength, bool bUpdateTileTextures = true);

	void UpdateTileTextures();

	GLEngine::Sprite* GetSpriteAtPixelLocation(const glm::vec2& loc);

	glm::vec2 GetTileLocationAtPixelPosition(const glm::vec2& loc);

	GLEngine::Sprite* GetSpriteAtGridLocation(const glm::vec2& loc);
	
	glm::vec3	GetTileMapOffset() const { return m_vTileMapOffset; }
	void				SetTileMapOffset(const glm::vec3& val);

	void						SetTileIDAt(const glm::vec2& val, unsigned int uiTileID);
	unsigned int	GetTileIDAt(const glm::vec2& val) const;

private:
	struct SpriteData
	{
		GLEngine::Sprite* pSprite;
		unsigned int uiTileID;
	};

	void CreateSprites();
	
	float m_fTileSize;

	float m_fMapWidth;
	float m_fMapHeight;

	unsigned int m_uiNumSpritesWidth;
	unsigned int m_uiNumSpritesHeight;
	unsigned int m_uiNumberTiles;
	
	std::map<unsigned int, unsigned int> m_tileIDToTextureIDMap;
	
	std::vector<SpriteData> m_tileSprites;
	GLEngine::SpriteRenderer* m_pRenderer;

	glm::vec3 m_vTileMapOffset;
	void UpdateTilePositions();
};