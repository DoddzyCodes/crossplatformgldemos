#include "AStarGrid.h"

#include "glm/glm.hpp"

template<int GridWidth, int GridHeight>
AStarGrid<GridWidth, GridHeight>::AStarGrid()
{
	ResetGrid(true);

}

template<int GridWidth, int GridHeight>
AStarGrid<GridWidth, GridHeight >::~AStarGrid()
{
}

template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::ResetGrid(bool bResetCost)
{
	//TODO: Update to memset
	for (int x = 0; x < GridWidth; ++x)
	{
		for (int y = 0; y < GridHeight; ++y)
		{
			GridData& data = m_pathFindGrid[x][y];
			data.pParent = nullptr;
			if(bResetCost) data.fCost = 5.0f;
			data.fDistance = 0.0f;
			data.fTotalGridValue = 0.0f;
			data.x = x;
			data.y = y;

			data.bIsOnOpenList = false;
			data.bIsOnClosedList = false;
		}
	}

	//Don't need to actually reset the lists, the Add functions will overwrite whatever used to be there
	m_uiOpenListSize = 0;
}

template<int GridWidth, int GridHeight>
inline bool AStarGrid<GridWidth, GridHeight>::FindPath(const glm::vec2 & start, const glm::vec2 & end, std::list<glm::vec2>& path)
{
	ResetGrid(false);

	//Add start node to open list

	//while open list isn't empty
	//Get best node from open list
	//if best node is end node return path
	//else
	//add connected nodes to open list (unless they are on closed list) or update their cost if better
	//add best node to closed list
	//end while
	assert(start.x >= 0 && start.x < GridWidth && start.y >= 0 && start.y < GridHeight && "Start position is not a valid grid point");
	assert(end.x >= 0 && end.x < GridWidth && end.y >= 0 && end.y < GridHeight && "End position is not a valid grid point");

	GridData* pStartNode = &m_pathFindGrid[(int)start.x][(int)start.y];
	GridData* pEndNode = &m_pathFindGrid[(int)end.x][(int)end.y];
	AddToOpenList(pStartNode);

	GridData* pCurrentNode = nullptr;
	//Keep looping until the open list is empty and returns null
	while(pCurrentNode = PopBestFromOpenList())
	{

		//If that node is the end, then build a path from it
		if (pCurrentNode == pEndNode)
		{
			BuildPath(path, pEndNode);
			return true;
		}


		//Otherwise, get all the nodes connected to this one
		GridData* connectedNodes[NUM_DIRECTIONS];
		GetConnectedNodes(pCurrentNode, connectedNodes);

		for (int i = 0; i < NUM_DIRECTIONS; ++i)
		{

			//If the node is invalid, or on the closed list, or has a negative travel cost, skip it
			GridData* pConnectedNode = connectedNodes[i];
			if (pConnectedNode == nullptr || pConnectedNode->fCost < 0 || IsOnClosedList(pConnectedNode))
			{
				continue;
			}

			//Manhattan distance
			float fDistance = (float)glm::sqrt((pEndNode->x - pConnectedNode->x) * (pEndNode->x - pConnectedNode->x) +
																	(pEndNode->y - pConnectedNode->y) * (pEndNode->y - pConnectedNode->y));

			//Extra cost if you have to go diagonally
			float fMoveCost = pConnectedNode->fCost;
			if (i == NORTHEAST || i == NORTHWEST || i == SOUTHEAST || i == SOUTHWEST) fMoveCost *= 1.44f;


			//Movement cost is F = G + H, where G is the cost to get here from the start location, and H is the distance to the target location
			float fTravelCost = pCurrentNode->fDistance + fMoveCost;
			float fTotalCost = fTravelCost + fDistance;

			//If the connected node hasn't been seen yet, or its more effecient to come from via the current node, update
			//its information
			if (!pConnectedNode->bIsOnOpenList || fTotalCost < pConnectedNode->fTotalGridValue)
			{
				pConnectedNode->pParent = pCurrentNode;
				pConnectedNode->fTotalGridValue = fTotalCost;
				pConnectedNode->fDistance = fTravelCost;
				if (pConnectedNode->bIsOnOpenList) RemoveFromOpenList(pConnectedNode);
				AddToOpenList(pConnectedNode);
			}

		}

		//Add the current node to the closed list so we can ignore it from future calculations
		AddToClosedList(pCurrentNode);
	};



	//If we hit here, then we didn't get to the end node, so return false
	return false;
}

template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::AddToOpenList(GridData* pData)
{
	assert(m_uiOpenListSize < GridWidth *  GridHeight && "Open list has filled up.  Shouldn't be trying to add to it");

	//Open list for the moment is simply an array that will fill up, with the highest index holding the smallest value
	//To keep it sorted, we place all new values at the end, and then bubble them down to their correct location


	m_openList[m_uiOpenListSize] = pData;
	pData->bIsOnOpenList = true;
	for (int uiIndex = (int)m_uiOpenListSize; uiIndex > 0;--uiIndex)
	{
		if (m_openList[uiIndex]->fTotalGridValue > m_openList[uiIndex - 1]->fTotalGridValue)
		{
			GridData* pTemp = m_openList[uiIndex];
			m_openList[uiIndex] = m_openList[uiIndex - 1];
			m_openList[uiIndex - 1] = pTemp;
		}
	}
	m_uiOpenListSize++;
}


template<int GridWidth, int GridHeight>
bool AStarGrid<GridWidth, GridHeight>::RemoveFromOpenList(GridData* data)
{
	//Search through the open list until we find the data we want to remove
	bool bMove = false;
	for (unsigned int uiIndex = 0; uiIndex < m_uiOpenListSize - 1; ++uiIndex)
	{
		if (m_openList[uiIndex] == data)
		{
			bMove = true;
			data->bIsOnOpenList = false;
		}

		//If we've found the data, we need to move everything down
		if (bMove)
		{
			GridData* pTemp = m_openList[uiIndex];
			m_openList[uiIndex] = m_openList[uiIndex + 1];
		}
	}

	if (bMove) m_uiOpenListSize--;

	return bMove; //bMove will be set to true if we find it
}

template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::AddToClosedList(GridData* data)
{
	data->bIsOnClosedList = true;
}

template<int GridWidth, int GridHeight>
bool AStarGrid<GridWidth, GridHeight>::IsOnClosedList(GridData* data)
{
	return data->bIsOnClosedList;
}

template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::BuildPath(std::list<glm::vec2>& path, GridData* pEndNode)
{
	GridData* pCurrentNode = pEndNode;
	while (pCurrentNode)
	{
		path.push_front(glm::vec2(pCurrentNode->x, pCurrentNode->y));
		pCurrentNode = pCurrentNode->pParent;
	}
}

template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::GetConnectedNodes(const GridData* pData, GridData** pNodeMap)
{
	memset(pNodeMap, 0, NUM_DIRECTIONS * sizeof(GridData*));

	if (pData->x > 0)
	{
		pNodeMap[WEST] = &m_pathFindGrid[pData->x - 1][pData->y];

		if (pData->y > 0) pNodeMap[NORTHWEST] = &m_pathFindGrid[pData->x - 1][pData->y - 1];
		if (pData->y < GridHeight - 1) pNodeMap[SOUTHWEST] = &m_pathFindGrid[pData->x - 1][pData->y + 1];
	}

	if (pData->x < GridWidth - 1)
	{
		pNodeMap[EAST] = &m_pathFindGrid[pData->x + 1][pData->y];

		if (pData->y > 0) pNodeMap[NORTHEAST] =& m_pathFindGrid[pData->x + 1][pData->y - 1];
		if (pData->y < GridHeight - 1) pNodeMap[SOUTHEAST] = &m_pathFindGrid[pData->x + 1][pData->y + 1];
	}
	if (pData->y > 0)
	{
		pNodeMap[NORTH] = &m_pathFindGrid[pData->x][pData->y - 1];
	}

	if (pData->y < GridHeight - 1)
	{
		pNodeMap[SOUTH] = &m_pathFindGrid[pData->x][pData->y + 1];
	}
}
template<int GridWidth, int GridHeight>
void AStarGrid<GridWidth, GridHeight>::SetTileCost(const glm::vec2& tile, float fCost)
{
	assert(tile.x >= 0 && tile.x < GridWidth && tile.y >= 0 && tile.y < GridHeight && "Tile Position is not a valid grid point");

	m_pathFindGrid[(int)tile.x][(int)tile.y].fCost = fCost;
}

template<int GridWidth, int GridHeight>
typename AStarGrid<GridWidth, GridHeight>::GridData* AStarGrid<GridWidth, GridHeight>::PopBestFromOpenList()
{
	if (m_uiOpenListSize == 0) return nullptr;

	GridData* pData = m_openList[m_uiOpenListSize - 1];
	m_uiOpenListSize--;

	return pData;
}
