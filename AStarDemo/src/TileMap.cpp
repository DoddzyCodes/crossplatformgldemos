#include "TileMap.h"
#include "SpriteRenderer.h"
#include "Sprite.h"
#include <iostream>

//TODO: Refactor and create a GetTileIndex() function

TileMap::TileMap(float fTileSize, float fMapWidth, float fMapHeight)
	: m_fTileSize(fTileSize)
	, m_fMapWidth(fMapWidth)
	, m_fMapHeight(fMapHeight)
	, m_pRenderer(nullptr)
{
	CreateSprites();
}

void TileMap::SetSpriteRenderer(class GLEngine::SpriteRenderer* pRenderer)
{
	GLEngine::SpriteRenderer* pOldRenderer = m_pRenderer;
	m_pRenderer = pRenderer;

	for (auto it = m_tileSprites.begin(); it != m_tileSprites.end(); ++it)
	{
		if (pOldRenderer) pOldRenderer->RemoveSprite(it->pSprite);
		m_pRenderer->AddSprite(it->pSprite);
	}
}

void TileMap::MapTileIDToTextureID(unsigned int uiTileID, unsigned int uiTextureID)
{
	m_tileIDToTextureIDMap[uiTileID] = uiTextureID;
}

void TileMap::SetTileIDs(const unsigned int* pTileIDs, unsigned int uiLength, bool bUpdateTileTextures)
{
	assert(uiLength == m_uiNumberTiles && "Make sure that the Tile ID array has the same number of elements as the total number of tiles");

	for (unsigned int i = 0; i < m_uiNumberTiles; ++i)
	{
		m_tileSprites[i].uiTileID = pTileIDs[i];
	}

	if(bUpdateTileTextures) UpdateTileTextures();
}

void TileMap::UpdateTileTextures()
{
	for (unsigned int i = 0; i < m_uiNumberTiles; ++i)
	{
		SpriteData& spr = m_tileSprites[i];
		unsigned int uiTextureID = m_tileIDToTextureIDMap[spr.uiTileID];
		m_tileSprites[i].pSprite->SetSpriteTexture(uiTextureID);
	}
}

GLEngine::Sprite* TileMap::GetSpriteAtPixelLocation(const glm::vec2& loc)
{
	glm::vec2 tileLocation = GetTileLocationAtPixelPosition(loc);

	if (tileLocation.x >= m_uiNumSpritesWidth) return nullptr;
	if (tileLocation.y >= m_uiNumSpritesHeight) return nullptr;

	return GetSpriteAtGridLocation(glm::vec2(tileLocation.x, tileLocation.y));
}

glm::vec2 TileMap::GetTileLocationAtPixelPosition(const glm::vec2& loc)
{
	float fHalfTileSize = m_fTileSize * 0.5f;
	unsigned int uiXPos = (unsigned int)((loc.x - m_vTileMapOffset.x + fHalfTileSize) / m_fTileSize);
	unsigned int uiYPos = (unsigned int)((loc.y - m_vTileMapOffset.y + fHalfTileSize) / m_fTileSize);

	return glm::vec2(uiXPos, uiYPos);
}

GLEngine::Sprite* TileMap::GetSpriteAtGridLocation(const glm::vec2& loc)
{
	unsigned int uiXPos = (unsigned int)loc.x;
	unsigned int uiYPos = (unsigned int)loc.y;

	return m_tileSprites[uiYPos * m_uiNumSpritesWidth + uiXPos].pSprite;
}

void TileMap::SetTileMapOffset(const glm::vec3& val)
{
	m_vTileMapOffset = val;
	UpdateTilePositions();
}

void TileMap::SetTileIDAt(const glm::vec2& val, unsigned int uiTileID)
{
	unsigned int uiIndex = (unsigned int)val.y * m_uiNumSpritesWidth + (unsigned int)val.x;
	assert(uiIndex <= m_uiNumberTiles && "You have passed in a invalid location!");

	m_tileSprites[uiIndex].uiTileID = uiTileID;

	unsigned int uiTextureID = m_tileIDToTextureIDMap[m_tileSprites[uiIndex].uiTileID];
	m_tileSprites[uiIndex].pSprite->SetSpriteTexture(uiTextureID);
}

unsigned int TileMap::GetTileIDAt(const glm::vec2& val) const
{
	unsigned int uiIndex = (unsigned int)val.y * m_uiNumSpritesWidth + (unsigned int)val.x;
	assert(uiIndex <= m_uiNumberTiles && "You have passed in a invalid location!");

	return m_tileSprites[uiIndex].uiTileID;
}

void TileMap::CreateSprites()
{
	assert(m_tileSprites.empty() && "Already created sprites for this tilemap");

	m_uiNumSpritesWidth = (int)(m_fMapWidth / m_fTileSize);
	m_uiNumSpritesHeight = (int)(m_fMapHeight / m_fTileSize);

	m_uiNumberTiles = m_uiNumSpritesWidth * m_uiNumSpritesHeight;
	m_tileSprites.resize(m_uiNumberTiles);


	for (unsigned int y = 0; y < m_uiNumSpritesHeight; ++y)
	{
		for (unsigned int x = 0; x < m_uiNumSpritesWidth; ++x)
		{
			m_tileSprites[y * m_uiNumSpritesWidth + x].pSprite =  new GLEngine::Sprite(m_fTileSize);

			GLEngine::Sprite* pSprite = m_tileSprites[y * m_uiNumSpritesWidth + x].pSprite;
			pSprite->SetColour(glm::vec4(1, 1, 1, 1));
			if (m_pRenderer) m_pRenderer->AddSprite(m_tileSprites[y * m_uiNumSpritesWidth + x].pSprite);
		}
	}

	UpdateTilePositions();
}

void TileMap::UpdateTilePositions()
{
	for (unsigned int y = 0; y < m_uiNumSpritesHeight; ++y)
	{
		for (unsigned int x = 0; x < m_uiNumSpritesWidth; ++x)
		{
			unsigned int uiIndex = y * m_uiNumSpritesWidth + x;
			m_tileSprites[uiIndex].pSprite->SetPosition(m_vTileMapOffset + glm::vec3(x * m_fTileSize, y * m_fTileSize, 0));
		}
	}
}

