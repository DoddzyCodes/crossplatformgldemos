project(3DDemo)

if(NOT EMSCRIPTEN)
    include_directories(${CMAKE_SOURCE_DIR}/deps/gl)
endif(NOT EMSCRIPTEN)


include_directories(${CMAKE_SOURCE_DIR}/deps/tinyobjloader)
include_directories(src)

SET(HEADERS
    src/GraphicsDemo.h
)

set(SOURCES
    src/GraphicsDemo.cpp
	src/main.cpp
)

add_executable(3DDemo ${SOURCES} ${HEADERS})
source_group(Include FILES ${HEADERS}) #This will format the output project file so the headers are placed seperately

#This project uses the GLCore
target_link_libraries (3DDemo GLCore)
