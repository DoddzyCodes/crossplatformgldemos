#include <stdio.h>
#include <stdlib.h>

#include "FlyCamera.h"
#include "GraphicsDemo.h"

int main(int argc, char **argv)
{
	GLEngine::FlyCamera camera(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	
	GraphicsDemo app(&camera);
	app.Run();

}