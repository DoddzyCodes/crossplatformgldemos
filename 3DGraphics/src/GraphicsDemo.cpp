#include "GraphicsDemo.h"
#include "PlatformDependant/glwrapper.h"

#include "RenderData.h"
#include "FlyCamera.h"

#include "glm/glm.hpp"
#include "glm/gtx/rotate_vector.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

#include <vector>
#include <string>
#include "glm/gtc/type_ptr.inl"
#include <intsafe.h>
#include <algorithm>

//TODO:  Write a proper shader loader

struct VertexData
{
	glm::vec3 position;
};

GraphicsDemo::GraphicsDemo(GLEngine::BaseCamera* pCamera) 
	: GLApplication("Graphics Demo", 1280, 720, pCamera)
	, m_modelRenderColour(0.8f, 0.8f, 0.8f, 1.0f)
{
	m_pCamera->SetupPerspective(3.1415f * 0.25f, (float)GetScreenWidth() / GetScreenHeight());

	m_modelTransform = glm::mat4(1);

}

bool GraphicsDemo::Startup()
{
	LoadModelFromObj("Data/Models/cube.obj");
	LoadSimpleShader();

	return true;

}

void GraphicsDemo::LoadModelFromObj(std::string strURI)
{
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	bool bLoaded = tinyobj::LoadObj(shapes, materials, err, strURI.c_str());
	assert(bLoaded && "Failed to load model");


	std::vector<float> fullVertexData(shapes[0].mesh.positions.size() + shapes[0].mesh.normals.size());
	auto it = std::copy(shapes[0].mesh.positions.begin(), shapes[0].mesh.positions.end(), fullVertexData.begin());
	std::copy(shapes[0].mesh.normals.begin(), shapes[0].mesh.normals.end(), it);

	//Need to convert unsigned int index into shorts since we don't support unsigned ints with emscripten
	std::vector<short> indexBuffer; indexBuffer.resize(shapes[0].mesh.indices.size());
	//This will be slow - write a binary loader
	//TODO: Write binary loader

	assert((shapes[0].mesh.positions.size() / 3) < SHORT_MAX && "Index size to large to store in a short!");
	for (int i = 0; i < shapes[0].mesh.indices.size(); ++i)
	{
		indexBuffer[i] = (short)shapes[0].mesh.indices[i];
	}


	m_pModelData = new  GLEngine::RenderData();
	m_pModelData->GenerateBuffers(GLEngine::RenderData::ALL);

	m_pModelData->Bind();

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(float) * fullVertexData.size(),
		fullVertexData.data(), GL_STATIC_DRAW);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(short) * indexBuffer.size(),
		indexBuffer.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(shapes[0].mesh.positions.size() * sizeof(float)));

	m_pModelData->SetIndexCount(shapes[0].mesh.indices.size());
	m_pModelData->SetNumberOfVertexAttributes(2);
	m_pModelData->SetIndexBufferType(GL_UNSIGNED_SHORT);

	m_pModelData->Unbind();
}

void GraphicsDemo::LoadSimpleShader()
{
	unsigned int uiShaderProgram;

	const char* vsSource =
		"attribute vec3 Position;\n"
		"attribute vec3 Normal;\n"
		"uniform mat4 projectionViewWorldMatrix;\n"
		"varying vec3 vNormal;"
		"void main(void) {\n"
		" gl_Position = projectionViewWorldMatrix * vec4(Position, 1);\n"
		" vNormal = (projectionViewWorldMatrix * vec4(Normal, 0)).xyz;"
		"}\n";
	const char* fsSource =
		"precision mediump float;"
		"uniform vec4 RenderColour;"
		"uniform vec3 LightDirection;"
		"uniform vec4 LightColour;"
		"varying vec3 vNormal;"
		"void main() {"
		"vec4 ambient = RenderColour;"
		"vec4 diffuse = clamp(dot(LightDirection, vNormal), 0.0, 1.0) * LightColour;"
		"gl_FragColor = ambient + diffuse;"
		"}";


	int success = GL_FALSE;
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);


	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	glCompileShader(vertexShader);

	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetShaderInfoLog(vertexShader, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!\n");
		printf("%s\n", infoLog);
	}

	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetShaderInfoLog(fragmentShader, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!\n");
		printf("%s\n", infoLog);
	}

	uiShaderProgram = glCreateProgram();

	glAttachShader(uiShaderProgram, vertexShader);
	glAttachShader(uiShaderProgram, fragmentShader);

	glBindAttribLocation(uiShaderProgram, 0, "Position");

	glLinkProgram(uiShaderProgram);

	glGetProgramiv(uiShaderProgram, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(uiShaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(uiShaderProgram, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!\n");
		printf("%s\n", infoLog);
		delete[] infoLog;
	}

	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);


	glUseProgram(uiShaderProgram);
	
	glUseProgram(0);


	m_uiShaderProgram = uiShaderProgram;
}

void GraphicsDemo::Shutdown()
{
}

bool GraphicsDemo::Update(double dt)
{
	glm::mat4 rot = glm::rotate(3.1415f * 0.5f * (float)dt, glm::vec3(0.0f, 1.0f, 0.0f));

	m_modelTransform *= rot;

	return true;
}

void GraphicsDemo::Render()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);

	glUseProgram(m_uiShaderProgram);
	unsigned int uiPVMLocation = glGetUniformLocation(m_uiShaderProgram, "projectionViewWorldMatrix");
	assert(uiPVMLocation != -1 && "Could not find PVM shader location");
	glUniformMatrix4fv(uiPVMLocation, 1, false, glm::value_ptr(m_pCamera->GetProjectionView() * m_modelTransform));

	unsigned int uiRenderColourLocation = glGetUniformLocation(m_uiShaderProgram, "RenderColour");
	assert(uiRenderColourLocation != -1 && "Could not find RenderColour shader location");
	glUniform4fv(uiRenderColourLocation, 1, glm::value_ptr(m_modelRenderColour));

	unsigned int uiLightColourLocation = glGetUniformLocation(m_uiShaderProgram, "LightColour");
	assert(uiLightColourLocation != -1 && "Could not find LightColour shader location");
	glUniform4fv(uiLightColourLocation, 1, glm::value_ptr(glm::vec4(1, 0, 0, 1)));


	unsigned int uiLightDirectionLocation = glGetUniformLocation(m_uiShaderProgram, "LightDirection");
	assert(uiLightDirectionLocation != -1 && "Could not find LightDirection shader location");
	glUniform3fv(uiLightDirectionLocation, 1, glm::value_ptr(glm::vec3(0.707, 0, 0.707)));

	m_pModelData->Bind();
	m_pModelData->Render();
	m_pModelData->Unbind();




}

