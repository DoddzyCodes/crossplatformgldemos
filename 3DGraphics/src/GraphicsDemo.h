#pragma once
#include "GLApplication.h"

#include "glm/vec4.hpp"
#include "glm/mat4x4.hpp"

namespace GLEngine { class RenderData; }

class GraphicsDemo : public GLEngine::GLApplication
{
public:
	GraphicsDemo(GLEngine::BaseCamera* pCamera);
	
protected:
	virtual bool Startup() override;



	virtual void Shutdown() override;


	virtual bool Update(double dt) override;


	virtual void Render() override;


private:
	void LoadModelFromObj(std::string strURI);
	void LoadSimpleShader();


	GLEngine::RenderData*	m_pModelData;
	unsigned int			m_uiShaderProgram;
	glm::vec4				m_modelRenderColour;

	glm::mat4				m_modelTransform;
};
